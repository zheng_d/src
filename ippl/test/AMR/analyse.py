##
# @file analyse.py
# @author Matthias Frey
# @date 26. Oct. 2016
# @details The files generated by error.cpp can be visualized using this script.
#          It plots the number of levels vs. the Euclidean error norm where the
#          error is computed between the single- and multi-level solution.
# @pre Environment variable OPAL_BUILD has to be set.
# @brief Plot the Euclidean error norm vs. the level of refinement


import numpy as np
import os
import matplotlib.pyplot as plt

## Absolute path to OPAL build directory
opal = os.environ['OPAL_BUILD']

##
# Create an error plot
# @param filename is the absolute path
# @param xlab is the label on the x-axis
# @param ylab is the label on the y-axis
def doPlot(filename, xlab, ylab):
    # error with respect to single-level solve.
    nLevels, l2err = np.loadtxt(filename, unpack=True)

    plt.figure()
    plt.plot(nLevels, l2err, "-o")
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.show()


doPlot(opal + "/ippl/test/AMR/l2_error_NOPARTICLES.dat",
       '#levels', r'$L_{2}$-error')

doPlot(opal + "/ippl/test/AMR/l2_error_UNIFORM.dat",
       '#levels', r'$L_{2}$-error')

doPlot(opal + "/ippl/test/AMR/l2_error_GAUSSIAN.dat",
       '#levels', r'$L_{2}$-error')
