// -*- C++ -*-
/***************************************************************************
 *
 *
 * EFFTPoissonSolver.cc
 *
 *
 *
 *
 *
 *
 *
 ***************************************************************************/

// include files
#include "Solvers/EFFTPoissonSolver.h"
#include "Algorithms/PartBunch.h"
#include "Physics/Physics.h"
#include <fstream>
//////////////////////////////////////////////////////////////////////////////
// a little helper class to specialize the action of the Green's function
// calculation.  This should be specialized for each dimension
// to the proper action for computing the Green's function.  The first
// template parameter is the full type of the Field to compute, and the second
// is the dimension of the data, which should be specialized.

#ifdef OPAL_NOCPLUSPLUS11_NULLPTR
#define nullptr NULL
#endif

template<unsigned int Dim>
struct SpecializedGreensFunction { };

template<>
struct SpecializedGreensFunction<3> {
    template<class T, class FT, class FT2>
    static void calculate(Vektor<T, 3> &hrsq, FT &grn, FT2 *grnI) {
        grn = grnI[0] * hrsq[0] + grnI[1] * hrsq[1] + grnI[2] * hrsq[2];
        grn = 1.0 / sqrt(grn);
        grn[0][0][0] = grn[0][0][1];
    }
};

////////////////////////////////////////////////////////////////////////////

// constructor
EFFTPoissonSolver::EFFTPoissonSolver(Mesh_t *mesh, FieldLayout_t *fl, std::string greensFunction, std::string bcz):
    mesh_m(mesh),
    layout_m(fl),
    mesh2_m(nullptr),
    layout2_m(nullptr),
    mesh3_m(nullptr),
    layout3_m(nullptr),
    mesh4_m(nullptr),
    layout4_m(nullptr),
    greensFunction_m(greensFunction)
{
    int i;
    domain_m = layout_m->getDomain();// Domain of the original rho field


     
    igrnexk0.initialize(*mesh_m, *layout_m);
    igrnexk1.initialize(*mesh_m, *layout_m);

    // For efficiency in the FFT's, we can use a parallel decomposition
    // which can be serial in the first dimension.
    e_dim_tag decomp[3];
    e_dim_tag decomp2[3];
    for(int d = 0; d < 3; ++d) {
        decomp[d] = layout_m->getRequestedDistribution(d);
        decomp2[d] = layout_m->getRequestedDistribution(d);
    }

    bcz_m = (bcz==std::string("PERIODIC"));   // for DC beams, the z direction has periodic boundary conditions

    if (bcz_m) { 
    
     INFOMSG("EFFTPoissonSolver does not support PERIODIC Boundary conditions so far ...!!!!!" << endl;);
    
    }
    // The FFT's require double-sized field sizes in order to (more closely
    // do not understand this ...)
    // simulate an isolated system.  The FFT of the charge density field, rho,
    // would otherwise mimic periodic boundary conditions, i.e. as if there were
    // several beams set a periodic distance apart.  The double-sized fields
    // alleviate this problem.
    // The double-sized field is doubled in x only!
    for(i = 0; i < 3; i++) {
        hr_m[i] = mesh_m->get_meshSpacing(i);
        nr_m[i] = domain_m[i].length();
     if (i == 0)    
    domain2_m[i] = Index(2 * nr_m[i] + 1);
    else
    domain2_m[i] = Index(nr_m[i] + 1);
    }
    // create double sized mesh and layout objects for the use in the FFT's
    mesh2_m = std::unique_ptr<Mesh_t>(new Mesh_t(domain2_m));
    layout2_m = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh2_m, decomp));
#ifdef OPAL_DKS
    rho2_m.initialize(*mesh2_m, *layout2_m, false);
#else
    rho2_m.initialize(*mesh2_m, *layout2_m);
#endif

    NDIndex<3> tmpdomain;
    // Create the domain for the transformed (complex) fields.  Do this by
    // taking the domain from the doubled mesh, permuting it to the right, and
    // setting the 2nd dimension to have n/2 + 1 elements.
    domain3_m[0] = Index(nr_m[3-1] + 1);
    domain3_m[1] = Index(nr_m[0] + 2);

    for(i = 2; i < 3; ++i)
        domain3_m[i] = Index(nr_m[i-1] + 1);

    // create mesh and layout for the new real-to-complex FFT's, for the
    // complex transformed fields
    mesh3_m = std::unique_ptr<Mesh_t>(new Mesh_t(domain3_m));
    layout3_m = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh3_m, decomp2));
    
    rho2tr_m.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m.initialize(*mesh3_m, *layout3_m);
    rho2tr_m01.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m01.initialize(*mesh3_m, *layout3_m);
    rho2tr_m10.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m10.initialize(*mesh3_m, *layout3_m);
    rho2tr_m11.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m11.initialize(*mesh3_m, *layout3_m);
    
    //grntr_m.initialize(*mesh3_m, *layout3_m);

    // helper field for sin
    //greentr_m.initialize(*mesh3_m, *layout3_m);


   //EFFT solver does not use the x, y, z order for Greens function calculation*/
    for(i = 0; i < 3; i++) {
        domain4_m[i] = Index(nr_m[i] + 2);
    }
  /*
    mesh4_m = std::unique_ptr<Mesh_t>(new Mesh_t(domain4_m));
    layout4_m = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh4_m, decomp));

    tmpgreen.initialize(*mesh4_m, *layout4_m);*/
  
   //EFFT solver usesthe z y x order at the beginning for Greens function calculation*/
        domain4_kji[0] = domain4_m[2];
        domain4_kji[1] = domain4_m[1];
        domain4_kji[2] = domain4_m[0];
    
    mesh4_kji = std::unique_ptr<Mesh_t>(new Mesh_t(domain4_kji));
    layout4_kji = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh4_kji, decomp));
    tmpgreen_kji.initialize(*mesh4_kji, *layout4_kji);
    
    igrn_kji.initialize(*mesh4_kji, *layout4_kji);

        domainCOSTConstruct_jik[0] = domain4_m[1];
        domainCOSTConstruct_jik[1] = domain4_m[0];
        domainCOSTConstruct_jik[2] = domain4_m[2];
    
      
     cost_m = std::unique_ptr<COS_t>(new COS_t(layout4_kji->getDomain(), domainCOSTConstruct_jik));// ???

        domain4_kij[0] = domain4_m[2];
        domain4_kij[1] = domain4_m[0];
        domain4_kij[2] = domain4_m[1]-1;
    
    mesh4_kij = std::unique_ptr<Mesh_t>(new Mesh_t(domain4_kij));
    layout4_kij = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh4_kij, decomp));
    igrn_kije.initialize(*mesh4_kij, *layout4_kij);
    igrn_kijo.initialize(*mesh4_kij, *layout4_kij);

    // create a domain used to indicate to the FFT's how to construct it's
    // temporary fields.  This is the same as the complex field's domain,
    // but permuted back to the left.
    tmpdomain = layout3_m->getDomain();
    for(i = 0; i < 3; ++i)
        domainFFTConstruct_m[i] = tmpdomain[(i+1) % 3];
    // create the FFT object
    fft_m = std::unique_ptr<FFT_t>(new FFT_t(layout2_m->getDomain(), domainFFTConstruct_m,
                              domainFFTConstruct_m, domainFFTConstruct_m, domainFFTConstruct_m));
    

    // these are fields that are used for calculating the Green's function.
    // they eliminate some calculation at each time-step.
    // ????
    for(i = 0; i < 3; ++i) {
        grnIField_m[i].initialize(*mesh2_m, *layout2_m);
        grnIField_m[i][domain2_m] = where(lt(domain2_m[i], nr_m[i]),
                                          domain2_m[i] * domain2_m[i],
                                          (2 * nr_m[i] - domain2_m[i]) *
                                          (2 * nr_m[i] - domain2_m[i]));
    }

}

////////////////////////////////////////////////////////////////////////////////
EFFTPoissonSolver::EFFTPoissonSolver(PartBunch &beam, std::string greensFunction):
    mesh_m(&beam.getMesh()),
    layout_m(&beam.getFieldLayout()),
    mesh2_m(nullptr),
    layout2_m(nullptr),
    mesh3_m(nullptr),
    layout3_m(nullptr),
    mesh4_m(nullptr),
    layout4_m(nullptr),
    greensFunction_m(greensFunction)
{
    int i;
    domain_m = layout_m->getDomain();// Domain of the original rho field

    INFOMSG("EFFTPoissonSolver(PartBunch &beam ..." << endl;);

     
    igrnexk0.initialize(*mesh_m, *layout_m);
    igrnexk1.initialize(*mesh_m, *layout_m);

    // For efficiency in the FFT's, we can use a parallel decomposition
    // which can be serial in the first dimension.
    e_dim_tag decomp[3];
    e_dim_tag decomp2[3];
    for(int d = 0; d < 3; ++d) {
        decomp[d] = layout_m->getRequestedDistribution(d);
        decomp2[d] = layout_m->getRequestedDistribution(d);
    }

    // The FFT's require double-sized field sizes in order to (more closely
    // do not understand this ...)
    // simulate an isolated system.  The FFT of the charge density field, rho,
    // would otherwise mimic periodic boundary conditions, i.e. as if there were
    // several beams set a periodic distance apart.  The double-sized fields
    // alleviate this problem.
    // The double-sized field is doubled in x only!
    for(i = 0; i < 3; i++) {
        hr_m[i] = mesh_m->get_meshSpacing(i);
        nr_m[i] = domain_m[i].length();
     if (i == 0)    
    domain2_m[i] = Index(2 * nr_m[i] + 1);
    else
    domain2_m[i] = Index(nr_m[i] + 1);
    }
    // create double sized mesh and layout objects for the use in the FFT's
    mesh2_m = std::unique_ptr<Mesh_t>(new Mesh_t(domain2_m));
    layout2_m = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh2_m, decomp));
#ifdef OPAL_DKS
    rho2_m.initialize(*mesh2_m, *layout2_m, false);
#else
    rho2_m.initialize(*mesh2_m, *layout2_m);
#endif

    NDIndex<3> tmpdomain;
    // Create the domain for the transformed (complex) fields.  Do this by
    // taking the domain from the doubled mesh, permuting it to the right, and
    // setting the 2nd dimension to have n/2 + 1 elements.
    domain3_m[0] = Index(nr_m[3-1] + 1);
    domain3_m[1] = Index(nr_m[0] + 2);

    for(i = 2; i < 3; ++i)
        domain3_m[i] = Index(nr_m[i-1] + 1);

    // create mesh and layout for the new real-to-complex FFT's, for the
    // complex transformed fields
    mesh3_m = std::unique_ptr<Mesh_t>(new Mesh_t(domain3_m));
    layout3_m = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh3_m, decomp2));
    
    rho2tr_m.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m.initialize(*mesh3_m, *layout3_m);
    rho2tr_m01.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m01.initialize(*mesh3_m, *layout3_m);
    rho2tr_m10.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m10.initialize(*mesh3_m, *layout3_m);
    rho2tr_m11.initialize(*mesh3_m, *layout3_m);
    imgrho2tr_m11.initialize(*mesh3_m, *layout3_m);
    
    //grntr_m.initialize(*mesh3_m, *layout3_m);

    // helper field for sin
    //greentr_m.initialize(*mesh3_m, *layout3_m);


   //EFFT solver does not use the x, y, z order for Greens function calculation*/
    for(i = 0; i < 3; i++) {
        domain4_m[i] = Index(nr_m[i] + 2);
    }
  /*
    mesh4_m = std::unique_ptr<Mesh_t>(new Mesh_t(domain4_m));
    layout4_m = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh4_m, decomp));

    tmpgreen.initialize(*mesh4_m, *layout4_m);*/
  
   //EFFT solver usesthe z y x order at the beginning for Greens function calculation*/
        domain4_kji[0] = domain4_m[2];
        domain4_kji[1] = domain4_m[1];
        domain4_kji[2] = domain4_m[0];
    
    mesh4_kji = std::unique_ptr<Mesh_t>(new Mesh_t(domain4_kji));
    layout4_kji = std::unique_ptr<FieldLayout_t>(new FieldLayout_t(*mesh4_kji, decomp));
    tmpgreen_kji.initialize(*mesh4_kji, *layout4_kji);
        domainCOSTConstruct_jik[0] = domain4_m[1];
        domainCOSTConstruct_jik[1] = domain4_m[0];
        domainCOSTConstruct_jik[2] = domain4_m[2];
    
      
     cost_m = std::unique_ptr<COS_t>(new COS_t(layout4_kji->getDomain(), domainCOSTConstruct_jik));// ???


    // create a domain used to indicate to the FFT's how to construct it's
    // temporary fields.  This is the same as the complex field's domain,
    // but permuted back to the left.
    tmpdomain = layout3_m->getDomain();
    for(i = 0; i < 3; ++i)
        domainFFTConstruct_m[i] = tmpdomain[(i+1) % 3];
    // create the FFT object
    fft_m = std::unique_ptr<FFT_t>(new FFT_t(layout2_m->getDomain(), domainFFTConstruct_m,
                              domainFFTConstruct_m, domainFFTConstruct_m, domainFFTConstruct_m));
    

    // these are fields that are used for calculating the Green's function.
    // they eliminate some calculation at each time-step.
    // ????
    for(i = 0; i < 3; ++i) {
        grnIField_m[i].initialize(*mesh2_m, *layout2_m);
        grnIField_m[i][domain2_m] = where(lt(domain2_m[i], nr_m[i]),
                                          domain2_m[i] * domain2_m[i],
                                          (2 * nr_m[i] - domain2_m[i]) *
                                          (2 * nr_m[i] - domain2_m[i]));
    }

#ifdef OPAL_DKS

    int dkserr;

    dksbase.setAPI("Cuda", 4);
    dksbase.setDevice("-gpu", 4);
    dksbase.initDevice();

    if (Ippl::myNode() == 0) {

      //create stream for greens function
      dksbase.createStream(streamGreens);
      dksbase.createStream(streamFFT);

      //create fft plans for multiple reuse
      int dimsize[3] = {2*nr_m[0], 2*nr_m[1], 2*nr_m[2]};
      
      dksbase.setupFFT(3, dimsize);
   
      //allocate memory
      int sizegreen = tmpgreen.getLayout().getDomain().size();
      int sizerho2_m = rho2_m.getLayout().getDomain().size();
      int sizecomp = grntr_m.getLayout().getDomain().size();

      tmpgreen_ptr = dksbase.allocateMemory<double>(sizegreen, dkserr);
      rho2_m_ptr = dksbase.allocateMemory<double>(sizerho2_m, dkserr);
      rho2real_m_ptr = dksbase.allocateMemory<double>(sizerho2_m, dkserr);

      grntr_m_ptr = dksbase.allocateMemory< std::complex<double>  >(sizecomp, dkserr);
      rho2tr_m_ptr = dksbase.allocateMemory< std::complex<double> > (sizecomp, dkserr);

      //send rho2real_m_ptr to other mpi processes
      //send streamFFT to other processes
      for (int p = 1; p < Ippl::getNodes(); p++) {
	dksbase.sendPointer( rho2real_m_ptr, p, Ippl::getComm() );
      }
    } else {
      //create stream for FFT data transfer
      dksbase.createStream(streamFFT);
      //receive pointer 
      rho2real_m_ptr = dksbase.receivePointer(0, Ippl::getComm(), dkserr);
    }
    
#endif

}

////////////////////////////////////////////////////////////////////////////
// destructor
EFFTPoissonSolver::~EFFTPoissonSolver() {
    // delete the FFT object
    //~ delete fft_m;

    // delete the mesh and layout objects
    //~ if(mesh2_m != 0)     delete mesh2_m;
    //~ if(layout2_m != 0)   delete layout2_m;
    //~ if(mesh3_m != 0)     delete mesh3_m;
    //~ if(layout3_m != 0)   delete layout3_m;

#ifdef OPAL_DKS
  //free all the allocated memory
  if (Ippl::myNode() == 0) {
    //get number of elements
    int sizegreen = tmpgreen.getLayout().getDomain().size();
    int sizerho2_m = rho2_m.getLayout().getDomain().size();
    int sizecomp = grntr_m.getLayout().getDomain().size();

    //free memory
    dksbase.freeMemory<double>(tmpgreen_ptr, sizegreen);
    dksbase.freeMemory<double>(rho2_m_ptr, sizerho2_m);
    dksbase.freeMemory< std::complex<double> >(grntr_m_ptr, sizecomp);

    //wait for other processes to close handle to rho2real_m_ptr before freeing memory
    MPI_Barrier(Ippl::getComm());
    dksbase.freeMemory<double>(rho2real_m_ptr, sizerho2_m);
    dksbase.freeMemory< std::complex<double> >(rho2tr_m_ptr, sizecomp);
  } else {
    dksbase.closeHandle(rho2real_m_ptr);
    MPI_Barrier(Ippl::getComm());
  }
#endif
}

////////////////////////////////////////////////////////////////////////////
// multiply the rho Field and the grn function for the efficient Poisson solver
//
void EFFTPoissonSolver::multiplyrhogrn(){
    
      submultiplyrhogrn(rho2tr_m,  rho2tr_m01,imgrho2tr_m,  imgrho2tr_m01,igrn_kije, igrnexk0, igrnexk1);
      submultiplyrhogrn(rho2tr_m10,rho2tr_m11,imgrho2tr_m10,imgrho2tr_m11,igrn_kijo, igrnexk0, igrnexk1);

}
////////////////////////////////////////////////////////////////////////////
// multiply the rho Field and the grn function for the efficient Poisson solver
//
void EFFTPoissonSolver::submultiplyrhogrn(
      CxField_t &rho0, CxField_t &rho1, 
      CxField_t &imgrho0, CxField_t &imgrho1, 
      Field_t &grn, Field_t &grnexk0, Field_t &grnexk1){

      int IdxEv;
      int IdxOd;
      int glength;
    
    NDIndex<3> idx =  layout3_m->getLocalNDIndex();
           glength = nr_m[2]+1; 

       for(int j = idx[2].first(); j <=  idx[2].last() + 1; j++) {
            for(int i = idx[1].first(); i <= idx[1].last() + 1; i++) {
                for(int k = idx[0].first(); k <= idx[0].last() + 1; k++) {
            IdxEv=(k<glength/2) ? 2*k : 2*(glength-k);
            IdxOd=(k<glength/2) ? 2*k+1 : 2*(glength-k)-1;
           grnexk0[k][i][j]=grn[IdxEv][i][j];
           grnexk1[k][i][j]=grn[IdxOd][i][j];
            }
           }
          }
      
     imgrho0=rho0*grnexk0;
     imgrho1=rho0*grnexk1;

}
////////////////////////////////////////////////////////////////////////////
// given a charge-density field rho and a set of mesh spacings hr,
// compute the electric field and put in eg by solving the Poisson's equation

void EFFTPoissonSolver::computePotential(Field_t &rho, Vector_t hr,double zshift) {


    INFOMSG("EFFTPoissonSolver does not support the z shift green's function so far ...!!!!" << endl;);

 }
void EFFTPoissonSolver::computePotential(Field_t &rho, Vector_t hr) {

    IpplTimings::startTimer(ComputePotential_m);



    // use grid of complex doubled in both dimensions
    // and store rho in lower  half of x-doubled grid
    rho2_m = 0.0;

    rho2_m[domain_m] = rho[domain_m];

    // needed in greens function
    hr_m = hr;

#ifndef OPAL_DKS
    // FFT double-sized charge density
    // we do a backward transformation so that we dont have to account for the normalization factor
    // that is used in the forward transformation of the IPPL FFT
  
    fft_m->transform("forward", rho2_m, rho2tr_m, rho2tr_m01, rho2tr_m10, rho2tr_m11);//forward is used fft_m->transform(-1, rho2_m, rho2tr_m);
    INFOMSG("*** DAWEI IS TESTING  line 471, computePotential at the EFFTPoissonSolver***"<< endl);

    // must be called if the mesh size has changed
    // have to check if we can do G with h = (1,1,1)
    // and rescale later
    IpplTimings::startTimer(GreensFunctionTimer_m);
    if(greensFunction_m == std::string("INTEGRATED"))
        effntintegratedGreensFunction();
    else
        effntintegratedGreensFunction();
        //greensFunction();
   
    INFOMSG("*** DAWEI IS TESTING  line 482, computePotential at the EFFTPoissonSolver***"<< endl);
     IpplTimings::stopTimer(GreensFunctionTimer_m);

    // multiply transformed charge density
    // and transformed Green function
    // Don't divide by (2*nx_m)*(2*ny_m), as Ryne does;
    // this normalization is done in POOMA's fft routine.
    INFOMSG("*** DAWEI IS TESTING  line 487, computePotential at the EFFTPoissonSolver***"<< endl);

    multiplyrhogrn();

    // inverse FFT, rho2_m equals to the electrostatic potential
    fft_m->transform("inverse", rho2tr_m, rho2tr_m01, rho2tr_m10, rho2tr_m11, rho2_m);

    // end convolution
#else

    dksbase.syncDevice();
    MPI_Barrier(Ippl::getComm());

    if (Ippl::myNode() == 0) {
      IpplTimings::startTimer(GreensFunctionTimer_m);
      integratedGreensFunction();
      IpplTimings::stopTimer(GreensFunctionTimer_m);
      //transform the greens function
      int dimsize[3] = {2*nr_m[0], 2*nr_m[1], 2*nr_m[2]}; 
      dksbase.callR2CFFT(rho2_m_ptr, grntr_m_ptr, 3, dimsize, streamGreens);
    }  
    MPI_Barrier(Ippl::getComm());

    //transform rho2_m keep pointer to GPU memory where results are stored in rho2tr_m_ptr  
    fft_m->transformDKSRC(-1, rho2_m, rho2real_m_ptr, rho2tr_m_ptr, dksbase, streamFFT, false);

    if (Ippl::myNode() == 0) {
      //transform the greens function
      //int dimsize[3] = {2*nr_m[0], 2*nr_m[1], 2*nr_m[2]}; 
      //dksbase.callR2CFFT(rho2_m_ptr, grntr_m_ptr, 3, dimsize, streamGreens);
      
      //multiply fields and free unneeded memory
      int sizecomp = grntr_m.getLayout().getDomain().size();
      dksbase.syncDevice();
      dksbase.callMultiplyComplexFields(rho2tr_m_ptr, grntr_m_ptr, sizecomp);
    }
   
    MPI_Barrier(Ippl::getComm());
  
    //inverse FFT and transfer result back to rho2_m
    fft_m->transformDKSCR(+1, rho2_m, rho2real_m_ptr, rho2tr_m_ptr, dksbase);

    MPI_Barrier(Ippl::getComm());
#endif

    // back to physical grid
    // reuse the charge density field to store the electrostatic potential
    rho[domain_m] = rho2_m[domain_m];

    IpplTimings::stopTimer(ComputePotential_m);
}
///////////////////////////////////////////////////////////////////////////
void EFFTPoissonSolver::effntintegratedGreensFunction() {

    IpplTimings::startTimer(IntGreensFunctionTimer1_m);
    INFOMSG("*** DAWEI IS TESTING  line 544, effntintegratedGreensFunction at the EFFTPoissonSolver***"<< endl);

    /**
     * This integral can be calculated analytically in a closed from:
     */

    NDIndex<3> idx =  layout4_kji->getLocalNDIndex();
    double cellVolume = hr_m[0] * hr_m[1] * hr_m[2];
    tmpgreen_kji = 0.0;

    INFOMSG("*** DAWEI IS TESTING  line 553, effntintegratedGreensFunction at the EFFTPoissonSolver***"<<idx[0].last()<< endl);

    for(int i = idx[2].first(); i <= idx[2].last() + 1; i++) {
        for(int j = idx[1].first(); j <=  idx[1].last() + 1; j++) {
            for(int k = idx[0].first(); k <= idx[0].last() + 1; k++) {

                Vector_t vv = Vector_t(0.0);
                vv(0) = i * hr_m[0] - hr_m[0] / 2;
                vv(1) = j * hr_m[1] - hr_m[1] / 2;
                vv(2) = k * hr_m[2] - hr_m[2] / 2;

                double r = sqrt(vv(0) * vv(0) + vv(1) * vv(1) + vv(2) * vv(2));
                double tmpgrn  = -vv(2) * vv(2) * atan(vv(0) * vv(1) / (vv(2) * r)) / 2;
                tmpgrn += -vv(1) * vv(1) * atan(vv(0) * vv(2) / (vv(1) * r)) / 2;
                tmpgrn += -vv(0) * vv(0) * atan(vv(1) * vv(2) / (vv(0) * r)) / 2;
                tmpgrn += vv(1) * vv(2) * log(vv(0) + r);
                tmpgrn += vv(0) * vv(2) * log(vv(1) + r);
                tmpgrn += vv(0) * vv(1) * log(vv(2) + r);
                tmpgreen_kji[k][j][i] = tmpgrn / cellVolume;// Be aware the index order is K, J, I

            }
        }
    }
    INFOMSG("*** DAWEI IS TESTING  line 577, effntintegratedGreensFunction at the EFFTPoissonSolver***"<< endl);
    IpplTimings::stopTimer(IntGreensFunctionTimer1_m);

    IpplTimings::startTimer(IntGreensFunctionTimer2_m);

    //assign seems to have problems when we need values that are on another CPU, i.e. [I+1]
    /*assign(rho2_m[I][J][K] ,
      tmpgreen[I+1][J+1][K+1] - tmpgreen[I][J+1][K+1] -
      tmpgreen[I+1][J][K+1] + tmpgreen[I][J][K+1] -
      tmpgreen[I+1][J+1][K] + tmpgreen[I][J+1][K] +
      tmpgreen[I+1][J][K] - tmpgreen[I][J][K]);*/

    Index I = nr_m[0] + 1;
    Index J = nr_m[1] + 1;
    Index K = nr_m[2] + 1;

    // the actual integration
    igrn_kji = 0.0;
    igrn_kji[K][J][I]  = tmpgreen_kji[K+1][J+1][I+1];
    igrn_kji[K][J][I] += tmpgreen_kji[K+1][J][I];
    igrn_kji[K][J][I] += tmpgreen_kji[K][J+1][I];
    igrn_kji[K][J][I] += tmpgreen_kji[K][J][I+1];
    igrn_kji[K][J][I] -= tmpgreen_kji[K+1][J+1][I];
    igrn_kji[K][J][I] -= tmpgreen_kji[K+1][J][I+1];
    igrn_kji[K][J][I] -= tmpgreen_kji[K][J+1][I+1];
    igrn_kji[K][J][I] -= tmpgreen_kji[K][J][I];

    IpplTimings::stopTimer(IntGreensFunctionTimer2_m);


    IpplTimings::startTimer(IntGreensFunctionTimer3_m);
    INFOMSG("*** DAWEI IS TESTING  line 606, effntintegratedGreensFunction at the EFFTPoissonSolver***"<< endl);
    
    cost_m->transform(-1, igrn_kji, igrn_kije, igrn_kijo); // The cosine transform of grn
    INFOMSG("*** DAWEI IS TESTING  line 609, effntintegratedGreensFunction at the EFFTPoissonSolver***"<< endl);
  
    IpplTimings::stopTimer(IntGreensFunctionTimer3_m);
}



Inform &EFFTPoissonSolver::print(Inform &os) const {
    os << "* ************* F F T P o i s s o n S o l v e r ************************************ " << endl;
    os << "* h " << hr_m << '\n';
    os << "* ********************************************************************************** " << endl;
    return os;
}

/***************************************************************************
 * $RCSfile:EFFTPoissonSolver.cc,v $   $Author: adelmann $ $Modified: Zheng$
 * $Revision: 1.6 $   $Date: 2016/11/4 13:03:08 $
 ***************************************************************************/
