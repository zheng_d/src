// -*- C++ -*-
/***************************************************************************
 *
 *
 * EFFTPoissonSolver.hh
 *
 * Open BC in x,y and z.
 *
 *
 *
 *
 *
 *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////
// This class contains methods for solving Poisson's equation for the
// space charge portion of the calculation.
////////////////////////////////////////////////////////////////////////////

#ifndef EFFT_POISSON_SOLVER_H_
#define EFFT_POISSON_SOLVER_H_


#ifdef dontOPTIMIZE_FIELD_ASSIGNMENT
#define FIELDASSIGNOPTIMIZATION __attribute__((optimize(0)))
#else
#define FIELDASSIGNOPTIMIZATION
#endif

#include <memory>
//////////////////////////////////////////////////////////////
#include "PoissonSolver.h"

#ifdef OPAL_DKS
#include "DKSBase.h"
#include "nvToolsExt.h"
#endif

class PartBunch;

//////////////////////////////////////////////////////////////

class EFFTPoissonSolver : public PoissonSolver {
public:
    // constructor and destructor
    EFFTPoissonSolver(PartBunch &bunch, std::string greensFuntion);

    EFFTPoissonSolver(Mesh_t *mesh, FieldLayout_t *fl, std::string greensFunction, std::string bcz); 

    ~EFFTPoissonSolver();

#ifdef OPAL_DKS
    DKSBase dksbase;
#endif

    // given a charge-density field rho and a set of mesh spacings hr,
    // compute the scalar potential with image charges at  -z
   
     void computePotential(Field_t &rho, Vector_t hr, double zshift); //invalid

    // given a charge-density field rho and a set of mesh spacings hr,
    // compute the scalar potential in open space
    void computePotential(Field_t &rho, Vector_t hr);

    // the multiply function for the efft Poisson solver
    void multiplyrhogrn();
 
    void submultiplyrhogrn(
      CxField_t &rho0, CxField_t &rho1,
      CxField_t &imgrho0, CxField_t &imgrho1,
      Field_t &grn, Field_t &grnexk0, Field_t &grnexk1);
 
    // compute the green's function for a Poisson problem and put it in in grntm_m
    // uses grnIField_m to eliminate excess calculation in greenFunction()
    // given mesh information in nr and hr
     
    // void greensFunction(); invalid

    /// compute the integrated Green function as described in <A HREF="http://prst-ab.aps.org/abstract/PRSTAB/v9/i4/e044204">Three-dimensional quasistatic model for high brightness beam dynamics simulation</A> by Qiang et al.
    //  void integratedGreensFunction(); invalid
    
    /// efficient integrated Green function 
    void effntintegratedGreensFunction();
    /// compute the shifted integrated Green function as described in <A HREF="http://prst-ab.aps.org/abstract/PRSTAB/v9/i4/e044204">Three-dimensional quasistatic model for high brightness beam dynamics simulation</A> by Qiang et al.
    //  void shiftedIntGreensFunction(double zshift); not valid

    double getXRangeMin() {return 1.0;}
    double getXRangeMax() {return 1.0;}
    double getYRangeMin() {return 1.0;}
    double getYRangeMax() {return 1.0;}
    double getZRangeMin() {return 1.0;}
    double getZRangeMax() {return 1.0;}
    void test(PartBunch &bunch) { }

    Inform &print(Inform &os) const;
private:

 //   void mirrorRhoField() FIELDASSIGNOPTIMIZATION;
 //   void mirrorRhoField(Field_t & ggrn2);// FIELDASSIGNOPTIMIZATION;

    // rho2_m is the charge-density field with mesh doubled in the first dimension
    Field_t rho2_m;

    // real field with layout of complex field: domain3_m
    Field_t greentr_m;

    // rho2tr_m is the Fourier transformed charge-density field
    // domain3_m and mesh3_ are used
    CxField_t rho2tr_m;
    CxField_t rho2tr_m01;
    CxField_t rho2tr_m10;
    CxField_t rho2tr_m11;
    
    CxField_t imgrho2tr_m;
    CxField_t imgrho2tr_m01;
    CxField_t imgrho2tr_m10;
    CxField_t imgrho2tr_m11;
     
    Field_t igrn_kji;
    Field_t igrn_kije;
    Field_t igrn_kijo;

    Field_t igrnexk0;//temp field, same size as original rho_m, with kij order
    Field_t igrnexk1;//temp field, same size as original rho_m

#ifdef OPAL_DKS
    //pointer for Fourier transformed Green's function on GPU
    void * grntr_m_ptr;
    void * rho2_m_ptr;
    void * tmpgreen_ptr;
    void *rho2real_m_ptr;
    void *rho2tr_m_ptr;

    //stream id for calculating greens function
    int streamGreens;
    int streamFFT;
  
#endif

    // Fields used to eliminate excess calculation in greensFunction()
    // mesh2_m and layout2_m are used
    IField_t grnIField_m[3];

    // the FFT object
    std::unique_ptr<FFT_t> fft_m;
    
    std::unique_ptr<COS_t> cost_m; 

    // mesh and layout objects for rho_m
    Mesh_t *mesh_m;
    FieldLayout_t *layout_m;

    // mesh and layout objects for rho2_m, double sized in the first dimension
    std::unique_ptr<Mesh_t> mesh2_m;
    std::unique_ptr<FieldLayout_t> layout2_m;

    //after the fft transform for rho2_m
    std::unique_ptr<Mesh_t> mesh3_m;
    std::unique_ptr<FieldLayout_t> layout3_m;

    // mesh and layout for integrated greens function
    std::unique_ptr<Mesh_t> mesh4_m;
    std::unique_ptr<FieldLayout_t> layout4_m;
    std::unique_ptr<Mesh_t> mesh4_kji;
    std::unique_ptr<FieldLayout_t> layout4_kji;
   // std::unique_ptr<Mesh_t> mesh4_jik;
   // std::unique_ptr<FieldLayout_t> layout4_jik;
    std::unique_ptr<Mesh_t> mesh4_kij;
    std::unique_ptr<FieldLayout_t> layout4_kij;

    // tmp
    Field_t tmpgreen_kji;

    // domains for the various fields
    NDIndex<3> domain_m;             // original domain, gridsize
    // mesh and gridsize defined outside of FFT class, given as
    // parameter to the constructor (mesh and layout object).
    NDIndex<3> domain2_m;            // doubled gridsize (2*Nx,Ny,Nz)
    NDIndex<3> domain3_m;            // field for the complex values of the RC transformation
    NDIndex<3> domain4_m;            // field for green function
    NDIndex<3> domain4_kji;            // field for green function
    //NDIndex<3> domain4_jik;            // field for green function temp
    NDIndex<3> domain4_kij;            // field for green function
    // (2*Nx,Ny,2*Nz)
    NDIndex<3> domainFFTConstruct_m;
    NDIndex<3> domainCOSTConstruct_jik;

    // mesh spacing and size values
    Vector_t hr_m;
    Vektor<int, 3> nr_m;

    /// for defining the boundary conditions
    BConds<double, 3, Mesh_t, Center_t> bc_m;
    BConds<Vector_t, 3, Mesh_t, Center_t> vbc_m;

    std::string greensFunction_m;

    bool bcz_m;

    IpplTimings::TimerRef GreensFunctionTimer_m;

    IpplTimings::TimerRef IntGreensFunctionTimer1_m;
    IpplTimings::TimerRef IntGreensFunctionTimer2_m;
    IpplTimings::TimerRef IntGreensFunctionTimer3_m;
    IpplTimings::TimerRef IntGreensMirrorTimer1_m;

    IpplTimings::TimerRef ShIntGreensFunctionTimer1_m;
    IpplTimings::TimerRef ShIntGreensFunctionTimer2_m;
    IpplTimings::TimerRef ShIntGreensFunctionTimer3_m;
    IpplTimings::TimerRef ShIntGreensFunctionTimer4_m;
    IpplTimings::TimerRef IntGreensMirrorTimer2_m;

    IpplTimings::TimerRef GreensFunctionTimer1_m;
    IpplTimings::TimerRef GreensFunctionTimer4_m;

    IpplTimings::TimerRef ComputePotential_m;
};

inline Inform &operator<<(Inform &os, const EFFTPoissonSolver &fs) {
    return fs.print(os);
}



#endif

/***************************************************************************
 * $RCSfile:EFFTPoissonSolver.hh,v $   $Author: adelmann $ $modified: Zheng$
 * $Revision: 1.1.1.1 $   $Date: 2016/11/04 13:05:08 $
 ***************************************************************************/
